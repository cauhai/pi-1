
const config = {
    entry: ['./index.js'],
    output: {
      path: __dirname + '/build',
      filename: 'pi-1.js'
    },
    module: {
      rules: [
        {
          test: /\.(js|jsx)$/,
          exclude: /node_modules/,
          loader: "babel-loader",
          options: { presets: ["@babel/env"] }
        },
        {
          test: /\.css$/,
          use: ["style-loader", "css-loader"]
        }
      ]
    },
    resolve: {
      extensions: ['.js']
    },
    devServer:{
      port: 3001,
      contentBase: __dirname + '/build',
      inline: true
    }
}
module.exports = config;
