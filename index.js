// The shell's index.html will load this script file via <script>
// Once loaded, this script file self-executes.

import { topElement } from './src/MainContents';
import './src/MainStyles.css';

// Add this plugin's own content to the shell
(() => {
  // The contract with the shell: add your own DOM to id="shell"
  let shell = document.getElementById('shell');
  shell.appendChild(topElement());
})();