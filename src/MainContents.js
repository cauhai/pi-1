// whole contents of the plugin, in the plugin's top element

function topElement() {
  let topElement = document.createElement('div');
  topElement.setAttribute('id', 'pi-1');
  topElement.classList.add('plugin');
  topElement.classList.add('pi-1');
  topElement.appendChild(mainContents());
  return topElement;
}

function mainContents() {
  // from here downwards, you can have components
  // example here is just a text node
  return document.createTextNode('PLUGIN #1, plain javascript.');
}

module.exports = { topElement }