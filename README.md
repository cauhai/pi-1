# pi-1

Plugin #1, plain javascript, no UI library.

## Local development
Clone repo
`npm install`
`npm start`
Visit http://localhost:3001

The top module is `src/MainContent.js`, from there downwards you add stuffs.

## Build and deploy
`npm run build` or `npm run build-prod`
Copy `build/pi-1.js` to some CDN site.


